﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movement : MonoBehaviour {

    public float speed;


    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKey ("s")) {
            transform.Translate (speed * Time.deltaTime, 0, 0);
	    }
        if (Input.GetKey ("w")) {
            transform.Translate (-speed * Time.deltaTime, 0, 0);
        }
        if (Input.GetKey ("d")) {
            transform.Translate (0, 0, speed * Time.deltaTime);
        }
        if (Input.GetKey ("a")) {
            transform.Translate (0, 0, -speed * Time.deltaTime);
        }
    }
}
     


