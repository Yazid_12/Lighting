﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Sword : MonoBehaviour

{
    //private Enemy enemyScript;
    public int attackDamage = 2;
    private BoxCollider swordCollider;

    void Start()
    {
        swordCollider = GetComponent<BoxCollider>();
    }

    public void Attack()
    {
        print("We are attacking!");

    }

    public void SwordHit()
    {
        swordCollider.enabled = true;
    }

    void OnTriggerEnter(Collider other)
    {
        if(other.tag == "Interactable")
        {
            other.GetComponent<Enemy>().TakeDamage(attackDamage);
            print("Enemy Taking Damage!");
            swordCollider.enabled = false;
        }
    }
}
